// This is the Pact verification test for order-service

const { Verifier } = require('@pact-foundation/pact')
const controller = require('../src/order/order.controller')
const Order = require('../src/order/order')

// Setup provider server to verify
const app = require('express')()
const authMiddleware = require('../src/middleware/auth.middleware')
// app.use(authMiddleware)
app.use(require('../src/order/order.routes'))
const server = app.listen("8080")

describe("Pact Verification", () => {
  it("validates the expectations of order-service", () => {
      const baseOpts = {
        logLevel: process.env.LOG_LEVEL || "INFO",
        providerBaseUrl: "http://localhost:8080",
        providerVersion: "1.0.0+localdev", // TODO: set this to a version derived from your CI build, for example with travis:  process.env.TRAVIS_COMMIT
        providerVersionTags: "LOCAL_DEV", // TODO: set this to the branch from your source control, for example with travis:  process.env.TRAVIS_BRANCH ? [process.env.TRAVIS_BRANCH] : [],
        verbose: process.env.VERBOSE === 'true',
        pactBrokerToken: 'HmsNdv9uBfSmUPteOhC1qQ',
        publishVerificationResult: true, //recommended to only publish from CI by setting the value to process.env.CI === 'true'
      }

      // For builds triggered by a 'contract content changed' webhook,
      // just verify the changed pact. The URL will bave been passed in
      // from the webhook to the CI job.
      const pactChangedOpts = {
        pactUrls: [process.env.PACT_URL]
      }

      // For 'normal' provider builds, fetch `master` and `prod` pacts for this provider
      const fetchPactsDynamicallyOpts = {
        provider: "order-service",
        consumerVersionSelectors: [{ tag: 'master', latest: true }, { tag: 'LOCAL_DEV', latest: true }, { tag: 'prod', latest: true } ],
        pactBrokerUrl: "https://promotion.pactflow.io",
        enablePending: false,
        includeWipPactsSince: undefined
      }

      // For information on provider states see https://docs.pact.io/getting_started/provider_states/
      // and https://docs.pact.io/provider/using_provider_states_effectively
      const stateHandlers = {
        // Provider state for when there are orders in the DB
        "orders exists": () => {
          controller.repository.orders = new Map([
            ["04", new Order('04', '7fy', 51.25, 5.25, 2, 5.1, 10.1)]
          ])
        },
        // Provider state for when order with ID 10 exists in the DB
        "i want to get one order": () => {
          controller.repository.orders = new Map([
            ["04", new Order('04', '7fy', 51.25, 5.25, 2, 5.1, 10.1)]
          ])
        },
        // Provider state for when order with ID 11 does not exist in the DB
        "a order with ID 11 does not exist": () => {
          controller.repository.orders = new Map()
        }
      }

      // Request filters allow you to amend the request made to the provider. We will use it here to
      // set a valid Authorization header
      //
      const requestFilter = (req, res, next) => {
        // WARNING: Do not modify anything else on the request, because you could invalidate the contract
        if (!req.headers["authorization"]) {
          next()
          return
        }
        req.headers["authorization"] = `Bearer ${new Date().toISOString()}`
        next()
      }

      const opts = {
        ...baseOpts,
        ...(process.env.PACT_URL ? pactChangedOpts : fetchPactsDynamicallyOpts),
        stateHandlers: stateHandlers,
        requestFilter: requestFilter
      }

      return new Verifier(opts).verifyProvider()
        .then(output => {
          console.log("Pact Verification Complete!")
        })
        .finally(() => {
          server.close()
        })
  })
})
