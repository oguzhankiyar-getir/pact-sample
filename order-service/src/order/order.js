class Order {
    constructor(id, restaurant, totalPrice, deliveryFee, deliveryType, minBasketSize, restaurantMinBasketSize) {
        this.id = id
        this.restaurant = restaurant
        this.totalPrice = totalPrice
        this.deliveryFee = deliveryFee
        this.deliveryType = deliveryType
        this.minBasketSize = minBasketSize
        this.restaurantMinBasketSize = restaurantMinBasketSize
    }
}

module.exports = Order
