// TODO: Replace these endpoints with your actual provider endpoints

const router = require('express').Router()
const controller = require('./order.controller')

router.get("/orders/:id", controller.getById)
router.get("/orders", controller.getAll)

module.exports = router
