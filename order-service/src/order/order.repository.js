const Order = require('./order')

class OrderRepository {

    constructor() {
        this.orders = new Map([
            ["01", new Order('01', '4fy', 51.25, 5.25, 2, 5.1, 10.1)],
            ["02", new Order('02', '5fy', 51.25, 5.25, 2, 5.1, 10.1)],
            ["03", new Order('03', '6fy', 51.25, 5.25, 2, 5.1, 10.1)],
            ["04", new Order('04', '7fy', 51.25, 5.25, 2, 5.1, 10.1)],
            ["05", new Order('05', '8fy', 51.25, 5.25, 2, 5.1, 10.1)],
        ])
    }

    async fetchAll() {
        return [...this.orders.values()]
    }

    async getById(id) {
        return this.orders.get(id)
    }
}

module.exports = OrderRepository
