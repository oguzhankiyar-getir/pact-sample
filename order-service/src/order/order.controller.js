const OrderRepository = require("./order.repository")

const repository = new OrderRepository()

exports.getAll = async (req, res) => {
    res.send(await repository.fetchAll())
}
exports.getById = async (req, res) => {
    const order = await repository.getById(req.params.id)
    order ? res.send(order) : res.status(404).send({message: "Order not found"})
}

exports.repository = repository
