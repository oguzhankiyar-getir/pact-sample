# order-service Pact example project using Jest and Express

This example is a really simple demonstration of the use of Pact in Jest tests.

Places where you need to make changes are marked with TODO comments

To run the tests:

```console
npm install
npm test
```


**REMEMBER to set the PACTFLOW_TOKEN environment variable with a valid token before running this!**


The important files in this project:

`order-service.js` - This starts up the provider express app.

`src/order/*` - These are the example API endpoints for a service that returns order data. Replace these
with your actual endpoints.

`__tests__/order-service.spec.js` - This is the Pact provider test.

  * When the `$PACT_URL` environment variable is not set (ie. the build is running because the provider changed), the provider is
  configured to fetch all the pacts for the 'order-service' provider which belong to the latest consumer
  versions tagged with master and prod. This ensures the provider is compatible with the latest changes that
  the consumer has made, and is also backwards compatible with the production version of the consumer.
  * When the `$PACT_URL` environment variable is set (ie. the build is running because it was triggered by
  the 'contract content changed' webhook), we just verify the pact at the `$PACT_URL`.
  * Pact-JS has a very flexible verification task configuration that allows us to use the same code for both
  the main pipeline verifications and the webhook-triggered verifications, with dynamically set options.
  Depending on your pact implementation, you may need to define separate tasks for each of these concerns.
  * When the verification results are published, the provider version number is set to the git sha, and the
  provider version tag is the git branch name. You can read more about versioning here. *You will need to
  set these based on your CI*

## Running Pact verifications

When using Pact in a CI/CD pipeline, there are two reasons for a pact verification task to take place:

  * When the provider changes (to make sure it does not break any existing consumer expectations)
  * When a pact changes (to see if the provider is compatible with the new expectations)

When the provider changes, the pact verification task runs as part the provider's normal build pipeline,
generally after the unit tests, and before any deployment takes place. This pact verification task is
configured to dynamically fetch all the relevant pacts for the specified provider from Pactflow,
verify them, and publish the results back to Pactflow.

To ensure that a verification is also run whenever a pact changes, you can create a webhook in Pactflow
that triggers a provider build, and passes in the URL of the changed pact. Ideally, this would be a
completely separate build from your normal provider pipeline, and it should just verify the changed pact.


**REMEMBER to set the PACTFLOW_TOKEN environment variable with a valid token before running this!**

