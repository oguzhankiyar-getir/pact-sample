"use strict"

// This is the Pact test for the evaluation-service

const { pactWith } = require("jest-pact")

// Load the consumer client code which we will call in our test
const { getOrder } = require("../evaluation-service")

pactWith({ consumer: "evaluation-service", provider: "order-service" }, provider => {
  // This is the body we expect to get back from the provider
  const EXPECTED_BODY = {
    id: '04',
    restaurant: '7fy',
    totalPrice: 51.25,
    deliveryFee: 5.25,
    deliveryType: 2,
    minBasketSize: 5.1,
    restaurantMinBasketSize: 10.1
  }

  describe("get /orders/04", () => {
    beforeEach(() => {
      const interaction = {
        state: "i want to get one order",
        uponReceiving: "a request for a single order",
        withRequest: {
          method: "GET",
          path: "/orders/04",
          headers: {
            Accept: "application/json",
          },
        },
        willRespondWith: {
          status: 200,
          headers: {
            "Content-Type": "application/json; charset=utf-8",
          },
          body: EXPECTED_BODY,
        },
      }

      return provider.addInteraction(interaction)
    })

    it("returns the correct response", () => {
      getOrder(provider.mockService.baseUrl).then(response => {
        return expect(response.data).toEqual(EXPECTED_BODY)
      })
    })
  })
})
