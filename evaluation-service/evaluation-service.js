"use strict"

const axios = require("axios")

// This is an example consumer that accesses the order-service via HTTP
// TODO: replace these functions with your actual ones

// Gets multiple entries from the order-service
exports.getOrders = endpoint => {
  return axios.request({
    method: "GET",
    baseURL: endpoint,
    url: "/orders",
    headers: { Accept: "application/json" },
  })
}

// Gets a single entry by ID from the order-service
exports.getOrder = endpoint => {
  return axios.request({
    method: "GET",
    baseURL: endpoint,
    url: "/orders/04",
    headers: { Accept: "application/json" },
  })
}
